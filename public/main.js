const textEditor = document.querySelector('.text-editor');
const preview = document.querySelector('.preview');
const button = document.querySelector('.save-button');
const converter = new showdown.Converter();

const renderPreview = value => {
    const html = converter.makeHtml(value);
    preview.innerHTML = html;
}

textEditor.addEventListener('keyup', evt => {
    const { value } =evt.target;

    window.localStorage.setItem('markdown',value);
    
    renderPreview(value);
    
});

const storedMarkdown = window.localStorage.getItem('markdown');

if(storedMarkdown){
    textEditor.value = storedMarkdown;
    renderPreview(storedMarkdown);
}

button.onclick = evt => {
    evt.preventDefault();
    var data = new FormData();
    const valueToSave = window.localStorage.getItem('markdown');
    data.append("data" , valueToSave);
    var xhr = new XMLHttpRequest();
    xhr.open( 'post', 'Files', true );
    xhr.send(data);
}

/* Botonera */
const h1 = document.querySelector(".btn-h1");
const h2 = document.querySelector(".btn-h2");
const h3 = document.querySelector(".btn-h3");
const h4 = document.querySelector(".btn-h4");
const code = document.querySelector(".code");
const link = document.querySelector(".link");
const image = document.querySelector(".image");


h1.onclick = evt => {
    textEditor.value = textEditor.value + "\n# set your title here ";
    renderPreview(textEditor.value);
}
h2.onclick = evt => {
    textEditor.value = textEditor.value + "\n## set your title here ";
    renderPreview(textEditor.value);
}
h3.onclick = evt => {
    textEditor.value = textEditor.value + "\n### set your title here ";
    renderPreview(textEditor.value);
}
h4.onclick = evt => {
    textEditor.value = textEditor.value + "\n#### set your title here ";
    renderPreview(textEditor.value);
}
code.onclick = evt => {
    textEditor.value = textEditor.value + "\n``` set your code here \n```";
    renderPreview(textEditor.value);
}
link.onclick = evt => {
    textEditor.value = textEditor.value + "\n[your text](your_link)";
    renderPreview(textEditor.value);
}
image.onclick = evt => {
    textEditor.value = textEditor.value + "\n<br>![Esta es una imagen](https://myoctocat.com/assets/images/base-octocat.svg)<br>";
    renderPreview(textEditor.value);
}